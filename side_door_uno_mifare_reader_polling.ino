#include <Wiegand.h>
#include <PubSubClient.h>
#include <WiFiEsp.h>
#ifndef HAVE_HWSERIAL1
#include "SoftwareSerial.h"
// Declare pins for 8266 serial communication
SoftwareSerial Serial1(7, 6); // RX, TX
#endif

// Include secrets file DEFINE all sensitive information
#include "arduino_secrets.h"

// These are the pins connected to the Wiegand D0 and D1 signals.
#define PIN_D0 2
#define PIN_D1 3

//Array of authorized card
const char* AuthorizedCards[] = {SECRET_CARD_0,SECRET_CARD_1};
const int numAuth = 2;

// WIFI Variables
char ssid[] = SECRET_SSID;            // your network SSID (name)
char pass[] = SECRET_WIFI_PASS;        // your network password
int status = WL_IDLE_STATUS;     // the Wifi radio's status


//MQTT server and creds
const char* server = SECRET_MQTT_SERVER;
const char* username = SECRET_MQTT_USER;
const char* password = SECRET_MQTT_PASS;
const char* UID = "arduino-2";
const char* mqttTopic = "rfid/side_door";

// The object that handles the wiegand protocol
Wiegand wiegand;

// Handler for MQTT received messages
void callback(char* topic, byte* payload, unsigned int length) {
  // handle message arrived
  // This software doesn't listen to MQTT
  // so this function is left empty
}

// Initialize ethernet and mqtt clients
WiFiEspClient ethClient;
PubSubClient client(server, 1883, callback, ethClient);

// Function to reconnect to MQTT is connection has dropped
boolean reconnect() {
  Serial.println("Reconnecting to MQTT server.");
  if (client.connect(UID, username, password)){
    Serial.println("Connected to MQTT");
  } else {
    Serial.println("Failed to connect to MQTT");
  }
}

// Initialize Wiegand reader
void setup() {
  Serial.begin(9600);

  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }

  // initialize serial for ESP module
  Serial1.begin(9600);

  //initialize pins as INPUT
  pinMode(PIN_D0, INPUT_PULLUP);
  pinMode(PIN_D1, INPUT_PULLUP);

  // initialize ESP module
  WiFi.init(&Serial1);

    // check for the presence of the shield
  if (WiFi.status() == WL_NO_SHIELD) {
    Serial.println("WiFi shield not present");
    // don't continue
    while (true);
  }

  // attempt to connect to WiFi network
  while ( status != WL_CONNECTED) {
    Serial.print("Attempting to connect to WPA SSID: ");
    Serial.println(ssid);
    // Connect to WPA/WPA2 network
    status = WiFi.begin(ssid, pass);
  }
  Serial.println("You're connected to the network");

  // Install listeners and initialize Wiegand reader
  wiegand.onReceive(receivedData, "Card readed: ");
  wiegand.onReceiveError(receivedDataError, "Card read error: ");
  wiegand.onStateChange(stateChanged, "State changed: ");
  wiegand.begin(Wiegand::LENGTH_ANY, false);

  // Initial connection to MQTT server
  reconnect();
}

// Continuously checks for pending messages and polls updates from the wiegand inputs
void loop() {
  // Checks for pending messages
  wiegand.flush();

  // Check for changes on the the wiegand input pins
  wiegand.setPin0State(digitalRead(PIN_D0));
  wiegand.setPin1State(digitalRead(PIN_D1));
}

// Notifies when a reader has been connected or disconnected.
// Instead of a message, the seconds parameter can be anything you want -- Whatever you specify on `wiegand.onStateChange()`
void stateChanged(bool plugged, const char* message) {
    Serial.print(message);
    Serial.println(plugged ? "CONNECTED" : "DISCONNECTED");
}

// Notifies when a card was read.
// Instead of a message, the seconds parameter can be anything you want -- Whatever you specify on `wiegand.onReceive()`
void receivedData(uint8_t* data, uint8_t bits, const char* message) {
    String CardCode = "";
    //Print value in HEX
    uint8_t bytes = (bits+7)/8;
    //this loop runs backwards as the reader is LSB
    //and most implants/cards are MSB
    for (int i=bytes-1; i>=0; i--) {
      // Copy HEX to variable CardCode
      String FirstNum = (String (data[i] >> 4, 16));
      String SecondNum = (String (data[i] & 0xF, 16));
      CardCode = (CardCode + FirstNum + SecondNum);
    }

    // Call authentication function with scanned card
    Serial.print("Scanned ");
    Serial.println(CardCode);
    authenticateCard(CardCode);
}

void authenticateCard(String CardHex) {
  // Loop through authorized cards to see if scanned card is authorized
  for (int i=0;i<numAuth;i++){
    if ( CardHex == AuthorizedCards[i] ) {
      // Card is authorized
      Serial.print(AuthorizedCards[i]);
      Serial.println(" is authorized.");

      // Reconnect to MQTT server if connection dropped
      if (!client.connected()) {
        Serial.println("MQTT not connected.");
        if (!reconnect()){
          Serial.println("Unable to connect to MQTT");
        } else {
          // Publish unlock message to MQTT broker
          client.publish(mqttTopic, "unlock");   
        }
      }

      //Exit loop since we've found the card
      i=numAuth;
    }
  }
}

// Notifies when an invalid transmission is detected
void receivedDataError(Wiegand::DataError error, uint8_t* rawData, uint8_t rawBits, const char* message) {
    Serial.print(message);
    Serial.print(Wiegand::DataErrorStr(error));
    Serial.print(" - Raw data: ");
    Serial.print(rawBits);
    Serial.print("bits / ");

    //Print value in HEX
    uint8_t bytes = (rawBits+7)/8;
    for (int i=0; i<bytes; i++) {
        Serial.print(rawData[i] >> 4, 16);
        Serial.print(rawData[i] & 0xF, 16);
    }
    Serial.println();
}
