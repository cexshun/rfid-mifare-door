# RFID Mifare Door

This project utilizes HID iClass SE readers to read mifare tags. This code is for LSB configured readers.

For some reason, any mini/micro/nano boards are not reliable. I had to use a full sized Arduino. Tested on a Duemilanove and an Uno with success.

The code authenticates the Mifare UID and submits data to a MQTT broker for processing via whatever method you choose. Works great in Home Assistant.

Configuration done via the variables in arduino_secrets.h
